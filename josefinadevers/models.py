from datetime import datetime

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from . import db


class Renter(db.Model):

    __tablename__ = 'renters'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(30), nullable=False)
    second_renter_name = db.Column(db.String(30))
    national_id = db.Column(db.String(30), nullable=False)
    phone = db.Column(db.Integer, nullable=False, default=10)
    email = db.Column(db.String(120))
    contract = db.relationship('Contract', backref='renters', lazy=True)


class Contract(db.Model):

    __tablename__ = 'contracts'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    contract_date = db.Column(db.DateTime, nullable=False,
                              default=datetime.now().strftime("%d-%m-%Y"))
    property_id = db.Column(db.Integer, nullable=False)
    contract_time = db.Column(db.Integer)
    contract_type = db.Column(db.String(45), nullable=False)
    fk_renter = db.Column(db.Integer,
                          db.ForeignKey('renters.id'),
                          nullable=False)
    fk_representative = db.Column(db.Integer,
                                  db.ForeignKey('representatives.id'),
                                  nullable=False)
    fk_property = db.Column(db.Integer,
                            db.ForeignKey('properties.id'),
                            nullable=False)
    charge = db.relationship(
        'Transaction', backref='charges', lazy=True)


class Representative(UserMixin, db.Model):
    """
    Create a Representative table
    """

    __tablename__ = 'representatives'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(30), nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    first_name = db.Column(db.String(30), nullable=False, index=True)
    last_name = db.Column(db.String(30), nullable=False, index=True)
    telephone = db.Column(db.String(15), nullable=False)
    contract = db.relationship(
        'Contract', backref='representatives', lazy=True)

    @property
    def password(self):
        """
        Prevent pasword from being accessed
        """
        raise AttributeError('password is not a readable attribute.')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)


class Deposit(db.Model):

    __tablename__ = 'deposits'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    amount_received = db.Column(db.Integer, nullable=False)
    amount_deducted = db.Column(db.Integer, nullable=False)
    fk_contract = db.Column(db.Integer,
                            db.ForeignKey('contracts.id'),
                            nullable=False)


class Property(db.Model):

    __tablename__ = 'properties'

    #  Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    property_type = db.Column(db.String(30), nullable=False)
    country = db.Column(db.String(50), nullable=False)
    location = db.Column(db.String(100), nullable=False)
    property_cost = db.Column(db.Integer, nullable=False)
    status = db.Column(db.String(20), nullable=False)
    contract = db.relationship(
        'Contract', backref='properties', lazy=True)


class Transaction(db.Model):

    __tablename__ = 'transactions'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    amount_received = db.Column(db.Integer, nullable=False)
    registry_date = db.Column(db.DateTime, nullable=False,
                              default=datetime.now().strftime("%d-%m-%Y"))
    comments = db.Column(db.String(255), nullable=False)
    fk_charge = db.Column(db.Integer,
                          db.ForeignKey('charges.id'),
                          nullable=False)


class Charge(db.Model):

    __tablename__ = 'charges'

    # Columns
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    charge_date = db.Column(db.DateTime, nullable=False)
    fk_contract = db.Column(db.Integer,
                            db.ForeignKey('contracts.id'),
                            nullable=False)
    transactions = db.relationship(
        'Transaction', backref='charges', lazy=True)
