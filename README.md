Renting department business application

Setting your environment
First activate virtualenv
Go to project path
python3 -m venv env
pip install --upgrade pip
pip install -U -r requirements.txt

# Create instance directory
Create config.py file inside instance directory with the following
FLASK_APP = "josefinadevers"
DEBUG = True
SQLALCHEMY_ECHO = True
SQLALCHEMY_TRACK_MODIFICATIONS = False

SECRET_KEY = ''
STRIPE_API_KEY = ''
SQLALCHEMY_DATABASE_URI= \
"dialect+driver://username:password@host:port/database"
